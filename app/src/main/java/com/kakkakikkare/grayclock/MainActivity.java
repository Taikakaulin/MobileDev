package com.kakkakikkare.grayclock;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Hashtable;
import java.util.Map;

import android.util.Log;

public class MainActivity extends Activity {

    // Tempo in milliseconds
    private int tempo = 1000;
    public int[] temp = {0,1,0,0,1,1};
    private CheckBox myCheckBox;
    private Vibrator myVibrator;
    SimpleDateFormat sdf = new SimpleDateFormat("h:mm");
    int s = Integer.parseInt(sdf.format(new Date()));
    Map<Integer, long[]> map = new HashMap<Integer, long[]>();
    map.Entry(1, eka[] );


    int dot = 200;
    int dash = 500;
    int short_gap = 200;
    int medium_gap = 500;
    int long_gap = 1000;
    long[] pattern ={
        dot, short_gap, dot, short_gap, dot,
            medium_gap,
            dash, short_gap, dash, short_gap, dash,
            medium_gap,
            dot, short_gap, dot, short_gap, dot, long_gap
    };
    long[] yksi ={
            dot, short_gap, dash, short_gap, dash, short_gap, dash, short_gap, dash, long_gap
    };
    long[] kaksi ={
            dot, short_gap, dot, short_gap, dash, short_gap, dash, short_gap, dash, long_gap
    };
    long[] kolme ={
            dot, short_gap, dot, short_gap, dot, short_gap, dash, short_gap, dash, long_gap
    };
    long[] nelja  ={
            dot, short_gap, dot, short_gap, dot, short_gap, dot, short_gap, dash, long_gap
    };
    long[] viisi ={
            dot, short_gap, dot, short_gap, dot, short_gap, dot, short_gap, dot, long_gap
    };
    long[] kuusi ={
            dash, short_gap, dot, short_gap, dot, short_gap, dot, short_gap, dot, long_gap
    };
    long[] seitseman ={
            dash, short_gap, dash, short_gap, dot, short_gap, dot, short_gap, dot, long_gap
    };
    long[] kahdeksan ={
            dash, short_gap, dash, short_gap, dash, short_gap, dot, short_gap, dot, long_gap
    };
    long[] yhdeksan ={
            dash, short_gap, dash, short_gap, dash, short_gap, dash, short_gap, dot, long_gap
    };
    long[] nolla ={
            dash, short_gap, dash, short_gap, dash, short_gap, dash, short_gap, dash, long_gap
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myCheckBox =(CheckBox) findViewById(R.id.checkBox);
        myVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);



    }




    private void Peep() {
        Pip1();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Pip2();
                // Actions to do after 1 second
            }
        }, 1000);
    }

    private void Pip1(){
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, ToneGenerator.MAX_VOLUME);
        toneG.startTone(ToneGenerator.TONE_PROP_BEEP, 100);
    }

    private void Pip2(){
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, ToneGenerator.MAX_VOLUME);
        toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 100);
    }

    Camera cam;
    void ledon() {
        cam = Camera.open();
        Camera.Parameters params = cam.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
        cam.setParameters(params);
        cam.startPreview();
        cam.autoFocus(new Camera.AutoFocusCallback() {
            public void onAutoFocus(boolean success, Camera camera) {
            }
        });
    }

    void ledoff() {
        cam.stopPreview();
        cam.release();
    }

    private void Flash() {
        ledon();
        // sleep
        //ledoff();

    }

    private void GetTime() {
        Date currentTime = Calendar.getInstance().getTime();

        // Format time to binary
    }

    public void StartPressed(View view) {
        if(myVibrator.hasVibrator() && myCheckBox.isChecked())
        {
            myVibrator.vibrate(pattern, -1);
        }
        //boolean vibrate = ((CheckBox)findViewById(R.id.checkBox)).isChecked();
        boolean Beep = ((CheckBox)findViewById( R.id.checkBox2)).isChecked();
        //Peep();
        boolean Flash = ((CheckBox)findViewById( R.id.checkBox3)).isChecked();

        int radioButtonId = ((RadioGroup)findViewById(R.id.radioGroup)).getCheckedRadioButtonId();




        //Morse();
        //Flash();
        //Peep();
    }
}
